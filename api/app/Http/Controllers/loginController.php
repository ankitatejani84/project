<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Log;
use JWTAuth;
use Config;
use JWTFactory;
use Response;
use App\Http\Requests;
use App\Jobs\EmailJob;
use Hash;

class loginController extends Controller
{
    /* /**
              * @api {post} project/api/public/doLoginForUser   user for doLoginForUser
              * @apiName doLoginForUser
              * @apiGroup user
              * @apiVersion 1.0.0
              * @apiSuccessExample Request-Header:
             {}
              * @apiSuccessExample Request-Body:
                {
"email_id":"ankita1@grr.la",
"password":"demo@123"
}

              * @apiSuccessExample Success-Response:
              {
    "code": "200",
    "message": "Login Success.",
    "cause": "",
    "data": {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQxLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L3Byb2plY3QvYXBpL3B1YmxpYy9kb0xvZ2luRm9yVXNlciIsImlhdCI6MTUxMDU3Mzg1MSwiZXhwIjoxNTEwNTc3NDUxLCJuYmYiOjE1MTA1NzM4NTEsImp0aSI6IldPQUVLbHBZS0x3NUNoVjgifQ.pfkTFgx-71fU2B8ikSL4e4aV_CAIMBH1FULM9HIMBAM",
        "user_details": [
            {
                "id": 41,
                "email_id": "ankita1@grr.la"
            }
        ]
    }
}
              */
    public function doLoginForUser(Request $request_body)
    {
        try {
            $request = json_decode($request_body->getContent());
            $response = (new VerificationController())->validateRequiredParameter(array('email_id', 'password'), $request);
            if ($response != '')
                return $response;

            $user_name = $request->email_id;
            $password = $request->password;
            $credential = ['email_id' => $user_name, 'password' => $password];
            if (!$token = JWTAuth::attempt($credential))
                return Response::json(array('code' => '201', 'message' => 'Invalid User Id or Password', 'cause' => '', 'data' => json_decode("{}")));

            DB::beginTransaction();
            $id = DB::table('user_master')->where('email_id', $user_name)->value('id');
            if ($id == 1) {
                return $response = Response::json(array('code' => '201', 'message' => 'user name or password is wrong.', 'cause' => '', 'data' => json_decode("{}")));
            }

            if (($response = (new VerificationController())->checkIfUserIsActive($id)) != '')
                return $response;

            $create_time = date("Y-m-d h:i:s");

            $is_exist = DB::select('select * from user_session WHERE user_id=?', [$id]);
            if ($is_exist == null) {
                DB::insert('INSERT INTO user_session
                                    (user_id, token)
                                    VALUES (?,?)',
                    [$id, $token]);
            } else {
                DB::table('user_session')->where('user_id', $id)->update(['token' => $token]);
            }

            DB::commit();


            $user_id = DB::table('user_master')->where('email_id', $user_name)->get(['id','email_id']);


            $result = ['token' => $token, 'user_details' => $user_id];
            DB::commit();

            $response = Response::json(array('code' => '200', 'message' => 'Login Success.', 'cause' => '', 'data' => $result));
        } catch (JWTException $e) {
            $response = Response::json(array('code' => '201', 'message' => 'Could not create token.', 'cause' => '', 'data' => json_decode("{}")));
            Log::error('doLoginForUser', ['JWTException' => $e->getMessage()]);
        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            Log::error('doLoginForUser', ['Exception' => $e->getMessage()]);
            DB::rollBack();
        }
        return $response;
    }


    // create new user session
    public function createNewSession($id, $token)
    {
        try {
            DB::beginTransaction();
            $create_time = date("Y-m-d h:i:s");
            DB::insert('INSERT INTO user_session
                                    (user_id, token)
                                    VALUES (?,?)',
                [$id, $token]);
            DB::commit();
            $response = '';

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => '', 'cause' => $e->getMessage(), 'data' => json_decode("{}")));
            Log::error('createNewSession', ['Exception' => $e->getMessage()]);
            DB::rollBack();
        }
        return $response;
    }

}
