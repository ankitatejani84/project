<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Config;
use ErrorException;
use QueryException;
use Exception;
use Response;
use Log;
use App\Jobs\EmailJob;
use Hash;
use JWTAuth;
use JWTFactory;
use Swift_TransportException;

class RegisterController extends Controller
{
    //------------------------------------------
    /* /**
          * @api {post} project/api/public/userSignUp   user for userSignUp
          * @apiName userSignUp
          * @apiGroup user
          * @apiVersion 1.0.0
          * @apiSuccessExample Request-Header:
         {}
          * @apiSuccessExample Request-Body:
            {
	"email":"ankita1@grr.la",
	"password":"demo@123"
}

          * @apiSuccessExample Success-Response:
          {
    "code": "200",
    "message": "You will be receiving sms shortly.",
    "cause": "",
    "data": {
        "user_reg_temp_id": 16,
        "otp_token": "269817"
    }
}

          */
    public function userSignUp(Request $request)
    {
        try {
            $request = json_decode($request->getContent());

            $response = (new VerificationController())->validateRequiredParameter(array('password', 'email'), $request);
            if ($response != '')
                return $response;

            $email = $request->email;

            //--------email verification-----
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $response = Response::json(array('code' => '201', 'message' => 'invalid email.', 'cause' => '', 'data' => ''));
                return $response;
            }
            $email = $request->email;
            $create_time = date("Y-m-d H:i:s");
            // var_dump($create_time);

            $response = (new VerificationController())->checkIfUserExist($email);
            if ($response != '')
                return $response;

            $otp_token = (new VerificationController())->generateOTP();
            $otp_token_expire = date('Y-m-d H:i:s', strtotime('+1 Hours', strtotime($create_time)));
            // var_dump($otp_token_expire);


            $request_json = json_encode($request);
            DB::beginTransaction();

            $data = array('user_name' => $email,
                'request_json' => $request_json,
            );
            $user_reg_temp_id = DB::table('user_registration_temp')->insertGetId($data);

            DB::insert('INSERT INTO otp_codes
                                    (user_registration_temp_id,otp_token,otp_token_expire, user_name)
                                    values (? ,? ,?, ?)',
                [$user_reg_temp_id, $otp_token, $otp_token_expire, $email]);

            DB::commit();

            $subject = 'OTP for Test';
            $template = 'promotional';
            $message_body = 'Thank You. You are on a way to registered with Test Your OTP is: ' . $otp_token;
            $api_name = 'doSignUpForUser';
            $api_description = 'Send mail for OTP verification.';
            $this->dispatch(new EmailJob($user_reg_temp_id, $email, $subject, $message_body, $template, $api_name, $api_description));

            $result = ['user_reg_temp_id' => $user_reg_temp_id,'otp_token'=>$otp_token];
            $response = Response::json(array('code' => '200', 'message' => 'You will be receiving sms shortly.', 'cause' => '', 'data' => $result));

        } catch (QueryException $e) {
            $response = Response::json(array('code' => '201', 'message' => 'Invalid query.', 'cause' => '', 'data' => json_decode("{}")));
            DB::rollBack();
        } catch (ErrorException $e) {
            $response = Response::json(array('code' => '201', 'message' => 'Error Exception' . $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            DB::rollBack();
        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => 'Exception' . $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            DB::rollBack();
        }
        return $response;
    }

    /* /**
          * @api {post} project/api/public/verifyOTPforUser   user for verifyOTPforUser
          * @apiName verifyOTPforUser
          * @apiGroup user
          * @apiVersion 1.0.0
          * @apiSuccessExample Request-Header:
         {}
          * @apiSuccessExample Request-Body:
            {
	"user_reg_temp_id":16,
	"otp_token":"269817"
}

          * @apiSuccessExample Success-Response:
          {
    "code": "200",
    "message": "You have successfully SignUp.",
    "cause": "",
    "data": {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQxLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L3Byb2plY3QvYXBpL3B1YmxpYy92ZXJpZnlPVFBmb3JVc2VyIiwiaWF0IjoxNTEwNTczNjg4LCJleHAiOjE1MTA1NzcyODgsIm5iZiI6MTUxMDU3MzY4OCwianRpIjoiaTRJV1lxaG1RUkQxcno2RSJ9.Xlm5WJbCtXR9adUvIo5YSVJZ2wk8HqeZiybD27EiE84"
    }
}

          */

    public function verifyOTPforUser(Request $request)
    {
        try {
            $request = json_decode($request->getContent());

            $response = (new VerificationController())->validateRequiredParameter(array('user_reg_temp_id', 'otp_token'), $request);
            if ($response != '')
                return $response;

            //Mandatory field
            $user_reg_temp_id = $request->user_reg_temp_id;
            $otp_token = $request->otp_token;

            $data = DB::select('select user_name, request_json from user_registration_temp where id=?', [$user_reg_temp_id]);
            $u_name = $data[0]->user_name;
            $uname = DB::select('select * from user_master where email_id=?', [$u_name]);
            if (sizeof($uname) > 0) {
                return $response = Response::json(array('code' => '201', 'message' => 'You have already verify your account.', 'cause' => '', 'data' => json_decode("{}")));
            }

            $response = (new VerificationController())->verifyOTP($user_reg_temp_id, $otp_token);
            if ($response != '')
                return $response;

            DB::beginTransaction();
             $result=$data[0]->request_json;
            if (count($result) != 0) {
                Log::info('result_info', ['response code' => count($result)]);
                $request = json_decode($result);
                //return $request->user_name;
                $user_master_data = array(
                    'email_id' => $request->email,
                    'password' => Hash::make($request->password),
                    'is_active' => 1,
                    'profile_setup' => 1,
                );

                $id = DB::table('user_master')->insertGetId($user_master_data);

                $user_details_data = array(
                    'user_id' => $id,
                );

                DB::table('user_details')->insert($user_details_data);

                $user_role_data = array(
                    'role_id' => Config::get('constant.ROLE_ID_FOR_USER'),
                    'user_id' => $id,
                );
                DB::table('role_user')->insert($user_role_data);

                $email = $request->email;
                $subject = 'Welcome to Test';
                $message_body = 'Thank you for signing up with test.';

                $template = 'promotional';
                $api_name = 'verifyOtpUser';
                $api_description = 'Send mail for successfully verification.';
                $this->dispatch(new EmailJob($user_reg_temp_id, $email, $subject, $message_body, $template, $api_name, $api_description));

                $create_time = date("Y-m-d h:i:s");
                $credential = ['email_id' => $request->email, 'password' => $request->password];
                if (!$token = JWTAuth::attempt($credential))
                    return Response::json(array('code' => '201', 'message' => 'Invalid User Id or Password', 'cause' => '', 'data' => json_decode("{}")));

                // Log::errorF("token:",$token);

                $token_verify = (new loginController())->createNewSession($id, $token);
                if ($token_verify != '')
                    return $token_verify;

                $result = ['token' => $token];
                $request = Response::json(array('code' => '200', 'message' => 'You have successfully SignUp.', 'cause' => '', 'data' => $result));
                DB::commit();
                // $request = Response::json(array('code' => '200', 'message' => 'You have successfully SignUp.', 'cause' => '', 'data' => $token));
                return $request;


            } else {
                $request = Response::json(array('code' => '201', 'message' => 'Invalid operation.user not found.', 'cause' => '', 'data' => json_decode("{}")));
                return $request;
            }

        } catch (QueryException $e) {
            $response = Response::json(array('code' => '201', 'message' => 'Invalid query.', 'cause' => $e->getMessage(), 'data' => json_decode("{}")));
            DB::rollBack();
        } catch (ErrorException $e) {
            $response = Response::json(array('code' => '201', 'message' => 'Invalid Operation.', 'cause' => $e->getMessage(), 'data' => json_decode("{}")));
            DB::rollBack();
        } catch (Swift_TransportException $e) {
            $response = Response::json(array('code' => '201', 'message' => 'Failed to send Email due to no internet.', 'cause' => 'Connection could not be established with host smtp.gmail.com ', 'data' => json_decode("{}")));
            DB::rollBack();
        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => $e->getMessage(), 'data' => json_decode("{}")));
            Log::error('verifyOTPforUser', ['Exception' => $e->getMessage()]);
            DB::rollBack();
        }
        return $response;
    }

}
