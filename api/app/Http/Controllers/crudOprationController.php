<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
Use DB;
use Response;

class crudOprationController extends Controller
{
    //
    /* /**
             * @api {post} project/api/public/categoryAdd   user for categoryAdd
             * @apiName categoryAdd
             * @apiGroup user
             * @apiVersion 1.0.0
             * @apiSuccessExample Request-Header:
            {
         Key: Authorization
         Value: Bearer token
         }
             * @apiSuccessExample Request-Body:
               //if you have NO child user
    {
"child_name":"test11"
}
    or

    //if you have child user
    {
"child_name":"test11",
"parent_id":1
}

             * @apiSuccessExample Success-Response:
             {
    "code": "200",
    "message": "Record inserted successfully.",
    "cause": "",
    "data": {}
}
             */
    public function categoryAdd(Request $request)
    {
        $request = json_decode($request->getContent());
        if (($response = (new VerificationController())->validateRequiredParameter(array('child_name'), $request)) != '')
            return $response;

        try{
            //if you use Middleware then remove this try catch
            try {
                $tokenFetch = JWTAuth::parseToken()->authenticate();
                if ($tokenFetch) {
                $token = str_replace("Bearer ", "", $request->header('Authorization'));
            } else {
                $token = '';
            }
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {//general JWT exception
            $token = '';
        }

         $parent_id=isset($request->parent_id) ? $request->parent_id : null;
            /*$max_id=DB::select('select MAX(id) as id from child');

           if($parent_id <= $max_id[0]->id)
               return Response::json(array('code' => '201', 'message' => 'This parent id is not exist..', 'cause' => '', 'data' => json_decode("{}")));*/
            DB::beginTransaction();

            if($parent_id==null){
                $parents=null;
            }else{
                $pre_parents=DB::table('child')->where('id', $parent_id)->get();
                if($pre_parents==[]){
                    $parents=$parent_id;
                }else{
                    $pre_parents=$pre_parents[0]->parents;
                    if($pre_parents==null)
                        $parents=$parent_id;
                    else
                        $parents=$pre_parents.'.'.$parent_id;
                }
            }

            $data=array(
                "parent_id"=>$parent_id,
                'child_name'=>$request->child_name,
                "parents"=>$parents
            );

            $child_table_id=DB::table('child')->insertGetId($data);

            /*if($parent_id==null){
                $parent_id=null;
            }else{

                $is_exist=DB::table('child_parents')->where('child_table_id', $parent_id)->get();
                if($is_exist==[]{
                    $parents=$is_exist[0]->parents;
                })else{

                }

                return $parents;
            }

            $child_parent_data=array(
                'child_table_id'=>$child_table_id,
                'parents'=>$parents
            );
            DB::table('child_parent');*/

            DB::commit();

            $response = Response::json(array('code' => '200', 'message' => 'Record inserted successfully.', 'cause' => '', 'data' => json_decode("{}")));

    }catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => $e->getMessage(), 'data' => json_decode("{}")));
            DB::rollBack();
    }
    return $response;
    }

    /* /**
             * @api {post} project/api/public/categoryUpdate   user for categoryUpdate
             * @apiName categoryUpdate
             * @apiGroup user
             * @apiVersion 1.0.0
             * @apiSuccessExample Request-Header:
            {
         Key: Authorization
         Value: Bearer token
         }
             * @apiSuccessExample Request-Body:
              {
"child_name":"testttt",
"id":8
}
             * @apiSuccessExample Success-Response:
             {
    "code": "200",
    "message": "Record updated successfully.",
    "cause": "",
    "data": {}
}
             */

    public function categoryUpdate(Request $request){
        $request = json_decode($request->getContent());
        if (($response = (new VerificationController())->validateRequiredParameter(array('id', 'child_name'), $request)) != '')
            return $response;

        try{
            //if you use Middleware then remove this try catch
            try {
                $tokenFetch = JWTAuth::parseToken()->authenticate();
                if ($tokenFetch) {
                    $token = str_replace("Bearer ", "", $request->header('Authorization'));
                } else {
                    $token = '';
                }
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {//general JWT exception
                $token = '';
            }

            $id=$request->id;
            $cat_name=$request->child_name;

            $is_exit=DB::table('child')->where('id', $id)->get();
            if(sizeof($is_exit)==0)
                return Response::json(array('code' => '201', 'message' => 'This category id is not exist.', 'cause' => '', 'data' => json_decode("{}")));

            DB::beginTransaction();

            DB::table('child')->where('id', $id)->update(['child_name' => $cat_name]);

            DB::commit();

            $response = Response::json(array('code' => '200', 'message' => 'Record updated successfully.', 'cause' => '', 'data' => json_decode("{}")));

        }catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => $e->getMessage(), 'data' => json_decode("{}")));
            DB::rollBack();
        }
        return $response;
    }

    /* /**
            * @api {post} project/api/public/categoryDelete   user for categoryDelete
            * @apiName categoryDelete
            * @apiGroup user
            * @apiVersion 1.0.0
            * @apiSuccessExample Request-Header:
           {
        Key: Authorization
        Value: Bearer token
        }
            * @apiSuccessExample Request-Body:
             {
"id":12
}
            * @apiSuccessExample Success-Response:
           {
    "code": "200",
    "message": "Record deleted successfully.",
    "cause": "",
    "data": {}
}
            */


    public function categoryDelete(Request $request){
        $request = json_decode($request->getContent());
        if (($response = (new VerificationController())->validateRequiredParameter(array('id'), $request)) != '')
            return $response;

        try{
            //if you use Middleware then remove this try catch
            try {
                $tokenFetch = JWTAuth::parseToken()->authenticate();
                if ($tokenFetch) {
                    $token = str_replace("Bearer ", "", $request->header('Authorization'));
                } else {
                    $token = '';
                }
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {//general JWT exception
                $token = '';
            }

            $id=$request->id;

            $is_exit=DB::table('child')->where('id', $id)->get();
            if(sizeof($is_exit)==0)
                return Response::json(array('code' => '201', 'message' => 'This category id is not exist.', 'cause' => '', 'data' => json_decode("{}")));

            DB::beginTransaction();

           $data=DB::table('child')->get();

           foreach ($data as $d){
               $data = explode('.',$d->parents);
               if (in_array($id, $data))
               {
                    DB::table('child')->where('id',$d->id)->delete();
               }
           }

            DB::table('child')->where('id', $id)->delete();

            DB::commit();

            $response = Response::json(array('code' => '200', 'message' => 'Record deleted successfully.', 'cause' => '', 'data' => json_decode("{}")));

        }catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => $e->getMessage(), 'data' => json_decode("{}")));
            DB::rollBack();
        }
        return $response;
    }

    /* /**
           * @api {post} project/api/public/CategoryList   user for CategoryList
           * @apiName CategoryList
           * @apiGroup user
           * @apiVersion 1.0.0
           * @apiSuccessExample Request-Header:
          {
       Key: Authorization
       Value: Bearer token
       }
           * @apiSuccessExample Request-Body:
            {
"id":12
}
           * @apiSuccessExample Success-Response:
          {
   "code": "200",
   "message": "Record deleted successfully.",
   "cause": "",
   "data": {}
}
           */

    public function CategoryList(Request $request){
        try{
            //if you use Middleware then remove this try catch
            try {
                $tokenFetch = JWTAuth::parseToken()->authenticate();
                if ($tokenFetch) {
                    $token = str_replace("Bearer ", "", $request->header('Authorization'));
                } else {
                    $token = '';
                }
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {//general JWT exception
                $token = '';
           }

            $Categorys = DB::table('child')->get();
            $tree='<ul id="browser" class="filetree">';
            //$tree='<ul id="browser" class="filetree"><li class="tree-view"></li>';
            foreach ($Categorys as $Category) {
                $tree .='<li class="tree-view closed"<a class="tree-name">'.$Category->child_name.'</a>';
                if($Category->parent_id!=null) {
                    $tree .=$this->childView($Category->parent_id);
                }
            }
            return $tree .='<ul>';

        }catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => $e->getMessage(), 'data' => json_decode("{}")));
            DB::rollBack();
        }
        return $response;
    }

    public function childView($Category_parent_id){
        //var_dump($Category_parent_id);
        $html ='<ul>';
        $value=DB::table('child')->where('id',$Category_parent_id)->get();
        $html .='<li class="tree-view"><a class="tree-name">'.$value[0]->child_name.'</a></li>';
        if(count($value[0]->parent_id))
            $html.= $this->childView($value[0]->parent_id);

        $html .="</ul>";
        return $html;
    }
}
