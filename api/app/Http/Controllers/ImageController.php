<?php

//////////////////////////////////////////////////////////////////////////////
//                   OptimumBrew Technology Pvt. Ltd.                       //
//                                                                          //
// Title:            suitescene                                             //
// File:             ImageController.php                                    //
// Since:            11-August-2016                                         //
//                                                                          //
// Author:           Dipali Dhanani                                         //
// Email:            dipali.dhanani@optimumbrew.com                         //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

namespace App\Http\Controllers;

use App\Http\Requests;
use Image;
use Exception;
use Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use File;
use Log;
use FFMpeg;
use Flash;

class ImageController extends Controller
{
    // get base url
    public function getBaseUrl(){
        return 'http://'.$_SERVER['HTTP_HOST'].'/'.Config::get('constant.PROJECT_NAME').'/api';
        //$a='http://'.$_SERVER['HTTP_HOST'].'/'.Config::get('constant.PROJECT_NAME');
        //var_dump($a);
    }

    //verify image
    public function verifyImage_for_autoAdminPanel($image_array){

         //Log::info('image_array12', ['data' => $image_array]);
        if(is_array($image_array)) {
            Flash::error('File should not be array.');
            return redirect(route('quoteMasters.create'));
         // return  $response = Response::json(array('code' => '201', 'message' => 'File should not be array', 'cause' => '', 'data' => json_decode("{}")));
        }

            $image_type = $image_array->getMimeType();
            $image_size = $image_array->getSize();

        $MAXIMUM_FILESIZE = 10 * 1024 * 1024;

        if(!($image_type == 'image/png' || $image_type == 'image/jpeg' || $image_type == 'image/gif')){
            Flash::error('Please select PNG or JPEG file.');
            return redirect(route('quoteMasters.index'));
        }
            //$response =  Response::json(array('code' => '201', 'message' => 'Please select PNG or JPEG file', 'cause'=>'','data'=>json_decode("{}")));
        elseif($image_size > $MAXIMUM_FILESIZE){
            Flash::error('File Size is greater then 10MB.');
            return redirect(route('quoteMasters.index'));
        }
            //$response =  Response::json(array('code' => '201', 'message' => 'File Size is greater then 5MB', 'cause'=>'','data'=>json_decode("{}")));
        else
            $response = '';
        return $response;
    }

    public function verifyVideo_for_autoAdminPanel($video_array){

        $video_type =$video_array->getMimeType();
        $video_size = $video_array->getSize();

        $MAXIMUM_FILESIZE = 10 * 1024 * 1024;

        //octet-stream ==>.3gp
        //quicktime ==>.mov

        if(!(/*$video_type == 'application/octet-stream'||*/ $video_type == 'video/x-ms-asf' || $video_type == 'video/mp4'|| $video_type == 'video/webm'|| $video_type == 'video/quicktime')){
            Flash::error('Please select 3gp or mp4 video file.');
            return redirect(route('quoteMasters.create'));
        }
           // $response =  Response::json(array('code' => '201', 'message' => 'Please select 3gp or mp4 file', 'cause'=>'','data'=>json_decode("{}")));
        elseif($video_size > $MAXIMUM_FILESIZE){
            Flash::error('File Size is greater then 15MB.');
            return redirect(route('quoteMasters.create'));
        }
            //$response =  Response::json(array('code' => '201', 'message' => 'File Size is greater then 15MB', 'cause'=>'','data'=>json_decode("{}")));
        else
            $response = '';
        return $response;
    }

    public function verifyAudio_for_autoAdminPanel($audio_array){

        $audio_type =$audio_array->getMimeType();
        $audio_size = $audio_array->getSize();

        $MAXIMUM_FILESIZE = 10 * 1024 * 1024;

        //octet-stream ==>.3gp
        //quicktime ==>.mov

        if(!($audio_type == 'audio/mpeg'||$audio_type == 'application/octet-stream')){
            Flash::error('Please select mp4 audio file.');
            return redirect(route('quoteMasters.index'));
        }
        // $response =  Response::json(array('code' => '201', 'message' => 'Please select 3gp or mp4 file', 'cause'=>'','data'=>json_decode("{}")));
        elseif($audio_size > $MAXIMUM_FILESIZE){
            Flash::error('File Size is greater then 5MB.');
            return redirect(route('quoteMasters.index'));
        }
        //$response =  Response::json(array('code' => '201', 'message' => 'File Size is greater then 15MB', 'cause'=>'','data'=>json_decode("{}")));
        else
            $response = '';
        return $response;
    }

    //generate image new name
    public function generateNewFileName($image_type,$image_array){

        $fileData = pathinfo(basename($image_array->getClientOriginalName()));
        $new_file_name =  uniqid().'_'.$image_type.'_'.time().'.'.$fileData['extension'];
        $path = Config::get('constant.ORIGINAL_IMAGES_DIRECTORY').$new_file_name;
        if(File::exists($path))
            $new_file_name =  uniqid().'_'.$image_type.'_'.time().'.'.$fileData['extension'];
        return $new_file_name;
    }
    public function generateNewthumnailFileName($image_type,$image_array){

        $fileData = pathinfo(basename($image_array->getClientOriginalName()));
        $new_file_name =  uniqid().'_'.$image_type.'_'.time().'.'.'png';
        $path = Config::get('constant.ORIGINAL_IMAGES_DIRECTORY').$new_file_name;
        if(File::exists($path))
            $new_file_name =  uniqid().'_'.$image_type.'_'.time().'.'.'png';
        return $new_file_name;
    }

    // Save original Image
    public function saveOriginalImage($img){
        $original_path = '../..'.Config::get('constant.ORIGINAL_IMAGES_DIRECTORY');
       $image= Input::file('file')->move($original_path,$img);
        //Log::error('saveOriginalImage', ['Exception' => $image]);
    }
    public function saveOriginalVideo($video){
        $original_path = '../..'.Config::get('constant.ORIGINAL_VIDEO_DIRECTORY');
        Input::file('file')->move($original_path, $video);
    }

    public function saveOriginalAudio($audio){
        $original_path = '../..'.Config::get('constant.ORIGINAL_AUDIO_DIRECTORY');
        Input::file('file')->move($original_path, $audio);
    }

    // Save encoded Image
    public function saveEncodedImage($image_array,$professional_img){
        $path = '../..'.Config::get('constant.ORIGINAL_IMAGES_DIRECTORY').$professional_img;
        file_put_contents($path, $image_array);
    }

    // Save Compressed Image
    public function saveCompressedImage($cover_img){
        try {
            Log::info('is_image',[$cover_img]);
            $original_path = '../..'.Config::get('constant.ORIGINAL_IMAGES_DIRECTORY').$cover_img;
            Log::info('original_path',[$original_path]);
            $compressed_path = '../..'.Config::get('constant.COMPRESSED_IMAGES_DIRECTORY').$cover_img;
            Log::info('compress_path',[$compressed_path]);
            $img = Image::make(file_get_contents($original_path));
            Log::info('Create_new_image_resource',['Exception' =>$img]);
            $img->save($compressed_path, 75);
            Log::info('Save image',['Exception' =>$img]);
        }catch (Exception $e){
            Log::error('saveCompressedImage', ['Exception' => $e->getMessage(),'Detail'=> $e->getTraceAsString()]);
            $dest1 = Config::get('constant.ORIGINAL_IMAGES_DIRECTORY').$cover_img;
            $dest2 = Config::get('constant.COMPRESSED_IMAGES_DIRECTORY').$cover_img;
            foreach($_FILES['file'] as $check){
                chmod($dest1,0777);
                copy($dest1, $dest2);
            }
        }
    }

    // Get Thumbnail Width Height
    public function getThumbnailWidthHeight($professional_img){

        $original_path = '../..'.Config::get('constant.ORIGINAL_IMAGES_DIRECTORY').$professional_img;
        $image_size = getimagesize($original_path);
        $width_orig = $image_size[0];
        $height_orig = $image_size[1];
        $ratio_orig = $width_orig / $height_orig;

        $width = $width_orig < Config::get('constant.THUMBNAIL_WIDTH') ? $width_orig : Config::get('constant.THUMBNAIL_WIDTH');
        $height = $height_orig < Config::get('constant.THUMBNAIL_HEIGHT') ? $height_orig : Config::get('constant.THUMBNAIL_HEIGHT');

        if ($width / $height > $ratio_orig)
            $width = $height * $ratio_orig;
        else
            $height = $width / $ratio_orig;

        $array = array('width'=>$width,'height'=>$height);
        return $array;
    }

    // Save Thumbnail Image
    public function saveThumbnailImage($professional_img){
        try {
            $array = $this->getThumbnailWidthHeight($professional_img);
            $width = $array['width'];
            $height = $array['height'];
            $original_path = '../..'.Config::get('constant.ORIGINAL_IMAGES_DIRECTORY').$professional_img;
            $thumbnail_path = '../..'.Config::get('constant.THUMBNAIL_IMAGES_DIRECTORY').$professional_img;
            Log::info('thumbnail_path',['Exception' =>$thumbnail_path]);
            $img = Image::make(file_get_contents($original_path))->resize($width, $height);
            Log::info('Create_new_image_thumb',['Exception' =>$img]);
            $img->save($thumbnail_path);
            Log::info('Save_image_thumb',['Exception' =>$img]);
        }catch (Exception $e){
            $dest1 = Config::get('constant.ORIGINAL_IMAGES_DIRECTORY').$professional_img;
            $dest2 = Config::get('constant.THUMBNAIL_IMAGES_DIRECTORY').$professional_img;
            foreach($_FILES['file'] as $check){
                chmod($dest1,0777);
                copy($dest1, $dest2);
            }
        }
    }

    // save Compressed and Thumbnail Image
    public function saveCompressedThumbnailImage($source_url, $destination_url,$thumbnail_path) {

        $info = getimagesize($source_url);
        $width_orig = $info[0];
        $height_orig = $info[1];
        $ratio_orig = $width_orig/$height_orig;

        $width = $width_orig < Config::get('constant.THUMBNAIL_WIDTH') ? $width_orig : Config::get('constant.THUMBNAIL_WIDTH');
        $height = $height_orig < Config::get('constant.THUMBNAIL_HEIGHT') ? $height_orig : Config::get('constant.THUMBNAIL_HEIGHT');

        if ($width/$height > $ratio_orig)
            $width = $height*$ratio_orig;
        else
            $height = $width/$ratio_orig;

        if ($info['mime'] == 'image/jpeg'){
            // save compress image
            $image = imagecreatefromjpeg($source_url);
            imagejpeg($image, $destination_url, 75);

            // save thumbnail image
            $tmp_img = imagecreatetruecolor( $width, $height );
            imagecopyresized( $tmp_img, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig );
            imagejpeg( $tmp_img,$thumbnail_path);

        }elseif ($info['mime'] == 'image/png'){
            // save compress image
            $image = imagecreatefrompng($source_url);
            imagepng($image, $destination_url, 9);

            // save thumbnail image
            $tmp_img = imagecreatetruecolor($width, $height);
            imagealphablending($tmp_img, false);
            imagesavealpha($tmp_img, true);
            $transparent = imagecolorallocatealpha($tmp_img, 255, 255, 255, 127);
            imagefilledrectangle($tmp_img, 0, 0, $width_orig, $height_orig, $transparent);
            imagecopyresized( $tmp_img, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig );
            imagepng( $tmp_img,$thumbnail_path);
        }
    }

    public function getThumbnailFromVideo($videoFile,$thumbnailFileName,$video_file_name){

        /*
         *  -i  Input file name
         *  -an Disable audio
         *  -ss Get image from X second in the video
         *  -s  Size of the image
         *
         */

        // For windows
//        $thumbnailFile='C:\wamp64\www\woc\image_bucket\thumbnail\\'.$thumbnailFileName;
//        $thumbnailSize="120*90";
//        $thumbnailSizeHD="240*180";
//        $getFromSecond=1;
//        $ffmpeg= "C:\\ffmpeg\\bin\\ffmpeg";
//        $cmd="$ffmpeg -i $videoFile -an -ss $getFromSecond -y -s $thumbnailSizeHD $thumbnailFile";
//        return (!exec($cmd));

        //For Linux
        /*$thumbnailFile='..\\image_bucket\\thumbnail\\".$thumbnailFileName';
        $cmd="ffmpeg -i $videoFile -an -ss $getFromSecond -y -s $thumbnailSize $thumbnailFile";
        return (!shell_exec($cmd));*/


        //From Composer Lib

        try{

          //local
            $ffmpeg = FFMpeg\FFMpeg::create(array(
                'ffmpeg.binaries'  => 'C:\\ffmpeg\\bin\\ffmpeg.exe',
                'ffprobe.binaries' => 'C:\\ffmpeg\\bin\\ffprobe.exe'));

            //linux server
//            $ffmpeg = FFMpeg\FFMpeg::create(array(
//                'ffmpeg.binaries'  => '/usr/bin/ffmpeg',
//                'ffprobe.binaries' => '/usr/bin/ffprobe'));
            $thumbnailFile1 = '../..'.Config::get('constant.ORIGINAL_VIDEO_DIRECTORY').$video_file_name;
          // $d = "$ffmpeg -i $thumbnailFile1 2>&1 | grep 'Duration'";

            $video = $ffmpeg->open($videoFile);
            $frame = $video->frame(FFMpeg\Coordinate\TimeCode::fromSeconds(0.05));

            $thumbnailFile = '../..'.Config::get('constant.THUMBNAIL_IMAGES_DIRECTORY').$thumbnailFileName;

            $frame->save($thumbnailFile);

           // unlink($thumbnailFile);
            if (file_exists($thumbnailFile)) {
                Log::info('add_image_thumbnail', ['thumbnail_name'=> $thumbnailFileName]);
                $response = 0;
            } else {
             //   $thumbnailFile1 = '../..'.Config::get('constant.ORIGINAL_VIDEO_DIRECTORY').$video_file_name;
                unlink($thumbnailFile1);
                Log::info('image_thumbnail_not_add', ['thumbnail_name'=> $thumbnailFileName]);
                $response =  1;

            }
        }catch(Exception $e) {
            Log::error('getThumbnailFromVideo', ['Exception' => $e->getMessage(),'Detail'=> $e->getTraceAsString()]);
            $response =  Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
        }
        return $response;
    }

}