<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Response;
use DB;
use Exception;
use Log;
use Config;
use Gidkom\OpenFireRestApi\OpenFireRestApi;
use JWTAuth;
use QueryException;
use Auth;
use TokenInvalidException;


class VerificationController extends Controller
{	
	 //get location address base on latitude and longitude
    public function getAddress($lat,$lng){
        try {
            $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($lng) . '&sensor=false';
            $json = @file_get_contents($url);
            $data = json_decode($json);
            $status = $data->status;
            if ($status == "OK") {
                return $data->results[0]->formatted_address;
            } else {
                return false;
            }
        }catch (Exception $e){
            Log::error('getAddress',['Exception'=>$e->getMessage()]);
        }
        return false;
    }
	
    // validate required and empty field
    public function validateRequiredParameter($required_fields, $request_params)
    {
        $error = false;
        $error_fields = '';

        foreach ($required_fields as $key => $value) {
            if (isset($request_params->$value)) {
                if (!is_object($request_params->$value)) {
                    if (strlen($request_params->$value) == 0) {
                        $error = true;
                        $error_fields .= ' ' . $value . ',';
                    }
                }
            } else {
                $error = true;
                $error_fields .= ' ' . $value . ',';
            }
        }

        if ($error) {
            // Required field(s) are missing or empty
            $error_fields = substr($error_fields, 0, -1);
            $message = 'Required field(s)' . $error_fields . ' is missing or empty.';
            $response = Response::json(array('code' => '201', 'message' => $message, 'cause' => '', 'data' => json_decode("{}")));
        } else
            $response = '';

        return $response;
    }

    public function validateRequiredArrayParameter($required_fields, $request_params)
    {
        $error = false;
        $error_fields = '';

        foreach ($required_fields as $key => $value) {
            if (isset($request_params->$value)) {
                if (!is_array($request_params->$value)) {
                    $error = true;
                    $error_fields .= ' ' . $value . ',';
                } else {
                    if (count($request_params->$value) == 0) {
                        $error = true;
                        $error_fields .= ' ' . $value . ',';
                    }
                }
            } else {
                $error = true;
                $error_fields .= ' ' . $value . ',';
            }
        }

        if ($error) {
            // Required field(s) are missing or empty
            $error_fields = substr($error_fields, 0, -1);
            $message = 'Required field(s)' . $error_fields . ' is missing or empty.';
            $response = Response::json(array('code' => '201', 'message' => $message, 'cause' => '', 'data' => json_decode("{}")));
        } else
            $response = '';

        return $response;
    }

    // validate required field
    public function validateRequiredParam($required_fields, $request_params)
    {
        $error = false;
        $error_fields = '';

        foreach ($required_fields as $key => $value) {
            if (!(isset($request_params->$value))) {
                $error = true;
                $error_fields .= ' ' . $value . ',';
            }
        }

        if ($error) {
            // Required field(s) are missing or empty
            $error_fields = substr($error_fields, 0, -1);
            $message = 'Required field(s)' . $error_fields . ' is missing.';
            $response = Response::json(array('code' => '201', 'message' => $message, 'cause' => '', 'data' => json_decode("{}")));
        } else
            $response = '';
        return $response;
    }

    // verify otp
    public function verifyOTP($registration_id, $otp_token)
    {
        try {
            $result = DB::select('SELECT otp_token_expire
                                  FROM otp_codes
                                  WHERE user_registration_temp_id = ? AND
                                        otp_token = ?', [$registration_id, $otp_token]);
            if (count($result) == 0) {
                $response = Response::json(array('code' => '201', 'message' => 'OTP is invalid.', 'cause' => '', 'data' => json_decode("{}")));
            } elseif (strtotime(date(Config::get('constant.DATE_FORMAT'))) > strtotime($result[0]->otp_token_expire)) {
                $response = Response::json(array('code' => '201', 'message' => 'OTP token expired.', 'cause' => '', 'data' => json_decode("{}")));
            } else {
                $response = '';
            }
        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            Log::error('verifyOTP', ['Exception' => $e->getMessage()]);
        }
        return $response;
    }

    public function verifyforgotOTP($user_name, $otp_token)
    {
        try {
            $result = DB::select('SELECT otp_token_expire
                                  FROM otp_codes
                                  WHERE user_name = ? AND
                                        otp_token = ?', [$user_name, $otp_token]);
            if (count($result) == 0) {
                $response = Response::json(array('code' => '201', 'message' => 'OTP is invalid.', 'cause' => '', 'data' => json_decode("{}")));
            } elseif (strtotime(date(Config::get('constant.DATE_FORMAT'))) > strtotime($result[0]->otp_token_expire)) {
                $response = Response::json(array('code' => '201', 'message' => 'OTP token expired.', 'cause' => '', 'data' => json_decode("{}")));
            } else {
                $response = '';
            }
        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            Log::error('verifyOTP', ['Exception' => $e->getMessage()]);
        }
        return $response;
    }

    // check if user is active
    public function checkIfUserIsActive($user_id)
    {
        try {

            $result = DB::select('SELECT
                                        um.is_active
                                        FROM user_master um
                                        WHERE um.id = ?', [$user_id]);
            $response = ($result[0]->is_active == '1') ? '' : Response::json(array('code' => '201', 'message' => 'You are inactive user. Please contact administrator.', 'cause' => '', 'data' => json_decode("{}")));

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
        }
        return $response;
    }

    public function checkIfUserIsBlock($user_id, $comp_receiver)
    {

        try {
            $result = DB::select('SELECT 1 FROM user_block WHERE user_id = ? and blocked_to_id = ? and record_action = 1', [$comp_receiver, $user_id]);
            $response = (sizeof($result) != 0) ? 1 : 0;

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            Log::error('checkIfUserIsBlock', ['Exception' => $e->getMessage()]);
        }
        return $response;
    }

    // check if user is Subscribed
    public function checkIfUserIsSubscribed($user_id)
    {
        try {
            $current_time = date("Y-m-d H:i:s");
            $result = DB::select('SELECT sub.user_id
                                        FROM subscriptions sub,
                                             user_master um
                                        WHERE sub.user_id=um.user_id AND
                                              sub.expiration_time > ? AND
                                              um.user_id = ?', [$current_time, $user_id]);


            if (count($result) == 0) {
                $reActivationURL = (new Utils())->getBaseUrl() . "/join/#/resubscribe/" . $user_id;
                $response = Response::json(array('code' => '402', 'message' => 'You subscription has been expired.', 'cause' => $reActivationURL, 'data' => json_decode("{}")));
            } else {
                $response = '';
            }

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => 'You subscription has been expired.', 'data' => json_decode("{}")));
        }
        return $response;
    }

    public function generateOTP()
    {
        $string = '0123456789';
        $string_shuffled = str_shuffle($string);
        $otp = substr($string_shuffled, 1, 6);
        return $otp;

    }

    // check if  user is active
    public function checkIfUserExist($email)
    {
        try {
            //Log::info($email);
            $email_result = DB::select('select * from user_master where email_id = ?', [$email]);
            //Log::error('result_info', ['response code' => $email_result]);


            if (sizeof($email_result) == 0)
                $response = '';
            else
                $response = Response::json(array('code' => '201', 'message' => 'User name OR email  already exist.', 'cause' => '', 'data' => json_decode("{}")));
        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => 'Exception' . $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            Log::error('checkIfUserExist', ['Exception' => $e->getMessage()]);
        }
        return $response;
    }

//check social
    //check if user is expire...................................function
    public function checkUserIsExpire($user_id)
    {
        try {
            $result = DB::select('select expiration_time from subscriptions where user_id = ?', [$user_id]);
            if(sizeof($result)>0){
                $exp_time=$result[0]->expiration_time;
                $current_time = date('Y-m-d H:i:s');
                if (strtotime($current_time) < strtotime($exp_time)){
                   // $response = Response::json(array('code' => '201', 'message' => 'your subscription time expired.', 'cause' => '', 'data' => json_decode("{}")));
                    return 0;
                }else{
                    return 1;
                }
            }else{
                return 1;
            }
           // $response = (sizeof($result) != 0) ? 1 : 0;

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            Log::error('checkUserIsExpire', ['Exception' => $e->getMessage()]);
        }
        //return $response;
    }


    public function checkIfEmailExist($email_id)
    {
        try {
            $result = DB::select('SELECT 1 FROM user_master WHERE email_id = ?', [$email_id]);
            $response = (sizeof($result) != 0) ? 1 : 0;

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            Log::error('checkIfUserExist', ['Exception' => $e->getMessage()]);
        }
        return $response;
    }


    public function checkIfProfileUserExist($user_name, $user_id)
    {
        try {
            $result = DB::select('SELECT 1 FROM user_master WHERE user_name = ? and id != ? ', [$user_name, $user_id]);
            $response = (sizeof($result) != 0) ? 1 : 0;

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            Log::error('checkIfUserExist', ['Exception' => $e->getMessage()]);
        }
        return $response;
    }

    public function checkIfProfileEmailExist($email_id, $user_id)
    {
        try {
            $result = DB::select('SELECT 1 FROM user_master WHERE email_id = ? and id != ? ', [$email_id, $user_id]);
            $response = (sizeof($result) != 0) ? 1 : 0;

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            Log::error('checkIfUserExist', ['Exception' => $e->getMessage()]);
        }
        return $response;
    }

    public function checkIfAdminId($comp_receiver, $user_id)
    {
        try {
            if ($comp_receiver == 1) {
                return $response = Response::json(array('code' => '201', 'message' => 'You are not eligible to sent compliment this user.', 'cause' => '', 'data' => json_decode("{}")));
            } else if ($user_id != '') {

                $result = DB::select('SELECT
                                        um.is_active
                                        FROM user_master um
                                        WHERE um.id = ?', [$user_id]);
                $response = ($result[0]->is_active == '1') ? '' : Response::json(array('code' => '401', 'message' => 'You are inactive user. Please contact administrator.', 'cause' => '', 'data' => json_decode("{}")));

            } else {
                $response = '';
            }

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            Log::error('checkIfAdmin', ['Exception' => $e->getMessage()]);
        }
        return $response;
    }

    public function invalidateUser($user_id)
    {
        try {

            $result = DB::select('SELECT
                                        um.is_active
                                        FROM user_master um
                                        WHERE um.id = ?', [$user_id]);
            $response = ($result[0]->is_active == '1') ? '' : Response::json(array('code' => '401', 'message' => 'You are inactive user. Please contact administrator.', 'cause' => '', 'data' => json_decode("{}")));

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
        }
        return $response;
    }

    // verify user
    public function verifyUser($user_id, $role_name)
    {
        try {
            $result = DB::select('SELECT r.name
                                  FROM role_user ru, roles r, user_master um
                                  WHERE r.id = ru.role_id AND
                                        um.id = ru.user_id AND
                                        um.id = ?', [$user_id]);
            $response = (sizeof($result) > 0 && $result[0]->name == $role_name) ? '' : Response::json(array('code' => '201', 'message' => 'Unauthorized user.', 'cause' => '', 'data' => json_decode("{}")));

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            Log::error('verifyUser', ['Exception' => $e->getMessage()]);
        }
        return $response;
    }

    // get user role
    public function getUserRole($user_id)
    {
        try {
            $result = DB::select('SELECT
                                        r.name
                                        FROM role_user ru, user_master um, roles r
                                        WHERE
                                          um.id = ru.user_id AND
                                          ru.role_id = r.id AND
                                          um.user_id = ?', [$user_id]);

            $response = (count($result) > 0) ? $result[0]->name : '';

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
        }
        return $response;
    }

    //same user
    public function ifSameUser($id_1, $id_2)
    {
        try {
            if ($id_1 == $id_2) {
                $response = "1";
            } else {
                $response = "0";
            }

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            Log::error('ifSameUser', ['Exception' => $e->getMessage()]);
        }
        return $response;
    }

    public function checkIfXMPPUserAvailable($userName)
    {
        try {

            // Create the Openfire Rest api object
            $api = new OpenFireRestApi();

            $api->secret = Config::get('constant.XMPP_REST_KEY');
            $api->host = Config::get('constant.XMPP_HOST');

            //Check if user is exits or not
            $result = $api->getUser($userName);

            if (!$result['status']) {
                throw new Exception("User Already Exits.");
            }

            return true;

        } catch (Exception $e) {
            Log::debug('checkIfXMPPUserAvailable', ['Exception userName:' . $userName => $e->getMessage()]);
            return false;
        }
    }

    public function disableXMPPUser($userName)
    {
        try {

            // Create the Openfire Rest api object
            $api = new OpenFireRestApi();

            $api->secret = Config::get('constant.XMPP_REST_KEY');
            $api->host = Config::get('constant.XMPP_HOST');

            //Check if user is exits or not
            $result = $api->lockoutUser($userName);

            if (!$result['status']) {
                throw new Exception("User Already Exits.");
            }
            return true;

        } catch (Exception $e) {
            Log::debug('checkIfXMPPUserAvailable', ['userName:' . $userName => $e->getMessage()]);
            return false;
        }
    }

    public function activateXMPPUser($userName)
    {
        try {

            // Create the Openfire Rest api object
            $api = new OpenFireRestApi();

            $api->secret = Config::get('constant.XMPP_REST_KEY');
            $api->host = Config::get('constant.XMPP_HOST');

            //Check if user is exits or not
            $result = $api->unlockUser($userName);

            if (!$result['status']) {
                throw new Exception("User Already Exits.");
            }

            return true;

        } catch (Exception $e) {
            Log::debug('checkIfXMPPUserAvailable', ['Exception userName:' . $userName => $e->getMessage()]);
            return false;
        }
    }

    //tesing api functions
    public function pushTesingForAndroid(Request $request_body)
    {
        try {
            $request = json_decode($request_body->getContent());
            if (($response = $this->validateRequiredParameter(array('token'), $request)) != '')
                return $response;

            $token = JWTAuth::getToken();
            JWTAuth::toUser($token);

            $token = $request->token;

            define('API_KEY', 'AAAARrposm8:APA91bGiCH8kpJn0ieaf06za2GI-M-BCTa8dXEsUiPALqTilcLzygtcgpIRZmC-gkQLr5OoEk57zytGC1sngPWPkKFVc-MWoWqd8Anxvzgo32STy8-XGEghOLHUHv_gcsYPIAva1jssen8gAITiDsc2u3obRpkwFBA');
            $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
            $message = 'hello';

            $fields = array(
                'to' => $token,
                'notification' => array('title' => 'tesing push', 'body' => $message),
                'data' => array('title' => 'tesing push', 'message' => $message)
            );

            $headers = array(
                'Authorization:key=' . API_KEY,
                'Content-Type:application/json'
            );
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);

            curl_close($ch);
            $response = Response::json(array('code' => '200', 'message' => 'successfully send push', 'cause' => '', 'data' => $result));


        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => '', 'cause' => 'unable to send push', 'data' => json_decode("{}")));
            Log::error('pushTesingForAndroid', ['Exception' => $e->getMessage()]);
        }
        return $response;


    }

    public function pushTesingForIos(Request $request_body)
    {
        try {
            $request = json_decode($request_body->getContent());
            if (($response = (new VerificationController())->validateRequiredParameter(array('push_type', 'token'), $request)) != '')
                return $response;

            $token = JWTAuth::getToken();
            JWTAuth::toUser($token);

            $token = $request->token;
            $push_type = $request->push_type;
            $title = "push testing";
            $message = "hello";


            $certificate_path_prof = app_path() . '/certificates/apns.pem';
            $deviceIds = (array)$token;
            header('content-type: text/html; charset: utf-8');
            $passphrase = 'WOC@2016';
            if ($push_type == '1') {
                $payload = '{"aps":{ "title":"' . $title . '", "alert":"' . $message . '","sound":"default", "thread-id": "WOC"}}';
            } else {
                $payload = '{
                 "aps":{ "title":"", "alert":"","sound":"","content-available" : 1, "thread-id": "","user_id": "", "extra_id": "", "messageType": "", "action_user_id": ""}
                }';
            }

            // start to create connection
            $ctx = stream_context_create();
            // stream_context_set_option($ctx, 'ssl', 'local_cert','../certi_bucket/ck.pem');
            stream_context_set_option($ctx, 'ssl', 'local_cert', $certificate_path_prof);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

            //echo count($deviceIds) . ' devices will receive notifications.<br />';

            foreach ($deviceIds as $item) {
                // wait for some time
                sleep(1);

                // Open a connection to the APNS server
                /*$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);*/
                $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

                if (!$fp) {
                    throw new Exception("Failed to connect: $err $errstr");
                    // exit("Failed to connect: $err $errstr" . '<br />');
                } else {
                    // echo 'Apple service is online. ' . '<br />';
                }

                // Build the binary notification
                $msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', $item)) . pack('n', strlen($payload)) . $payload;
                //$msg = chr(0) . pack('n', 32) . pack('H*',$item) . pack('n', strlen($payload)) . $payload;

                // Send it to the server
                $result = fwrite($fp, $msg, strlen($msg));

                if (!$result) {
                    //  echo 'Undelivered message count: ' . $item . '<br />';
                    Log::info('sendMessageToIOS', ['result' => 'Undelivered message To: ' . $item]);

                } else {
                    Log::info('sendMessageToIOS', ['result' => 'Delivered message To: ' . $item]);
                }

                if ($fp) {
                    fclose($fp);
                    // echo 'The connection has been closed by the client' . '<br />';
                }
            }

            // echo count($deviceIds) . ' devices have received notifications.<br />';

            // set time limit back to a normal value
            set_time_limit(30);
            $response = Response::json(array('code' => '200', 'message' => '', 'cause' => 'successfully send push', 'data' => $result));


        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => '', 'cause' => 'unable to send push', 'data' => json_decode("{}")));
            Log::error('pushTesingForIos', ['Exception' => $e->getMessage()]);
        }
        return $response;


    }

    public function testForVideoUpload(Request $request_body)
    {
        try {
            if ($request_body->hasFile('file')) {
                $video_array = Input::file('file');
                if (($response = (new ImageController())->verifyVideo($video_array)) != '')
                    return $response;
            }

            $token = JWTAuth::getToken();
            JWTAuth::toUser($token);

            $comp_video = (new ImageController())->generateNewFileName('compliment_video', $video_array);
            $thum_video = (new ImageController())->generateNewthumnailFileName('compliment_video_thum', $video_array);
            //return $comp_video['video'];
            $base_url = (new Utils())->getBaseUrl() . Config::get('constant.ORIGINAL_IMAGES_DIRECTORY') . '/' . $comp_video;
            (new ImageController())->saveOriginalVideo($comp_video);

            (new ImageController())->getThumbnailFromVideo($base_url, $thum_video);

            $response = Response::json(array('code' => '200', 'message' => 'video upload successfully!.', 'cause' => '', 'data' => $comp_video));

        } catch (Exception $e) {
            $response = Response::json(array('code' => '201', 'message' => $e->getMessage(), 'cause' => '', 'data' => json_decode("{}")));
            Log::error('addCompliment', ['Exception' => $e->getMessage()]);
            DB::rollBack();
        }
        return $response;
    }
}
