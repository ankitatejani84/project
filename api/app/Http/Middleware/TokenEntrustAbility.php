<?php

namespace App\Http\Middleware;

use Closure;
use Mockery\CountValidator\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Middleware\BaseMiddleware;
use Response;
use JWTAuth;
use Log;

class TokenEntrustAbility extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles, $permissions, $validateAll = false)
    {
        //check token is missing or not.
        if (! $token = $this->auth->setRequest($request)->getToken())
            return Response::json(array('code'=>'201','message'=>'Required field token is missing or empty.','cause'=>'','data'=> json_decode("{}")));

        try {
                $user = $this->auth->authenticate($token);

        } catch (TokenExpiredException $e) {
            try{
                $new_token = ['new_token' =>JWTAuth::refresh($token)];
            }catch (TokenExpiredException $e){
              //  Log::info('TokenExpiredException Can not be Refresh',['status_code'=>$e->getStatusCode()]);
                return Response::json(array('code' => $e->getStatusCode(), 'message' => $e->getMessage(),'cause'=>'','data' => json_decode('{}')));
            }
            return Response::json(array('code' => $e->getStatusCode(), 'message' => 'Token expired.','cause'=>'','data' => ['new_token' => $new_token]));
            } catch (JWTException $e) {
            return Response::json(array('code' => $e->getStatusCode(), 'message' => $e->getMessage(),'cause'=>'','data' => json_decode("{}")));
        }

        if (! $user)
            return Response::json(array('code' => '404', 'message' => 'User not found.', 'cause' => '', 'data' => json_decode("{}")));

        if (!$request->user()->ability(explode('|', $roles), explode('|', $permissions), array('validate_all' => $validateAll)))
            return Response::json(array('code' => '201', 'message' => 'Unauthorized user.', 'cause' => '', 'data' => json_decode("{}")));

        $this->events->fire('tymon.jwt.valid', $user);

        return $next($request);
    }
}
