<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;  charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>test</title>
    <style>
        .left{
            float: left;
            width: 50%;
            text-align: center;
            padding-top: 50px;
        }
        .right{
            float: right;
            width: 50%;
            text-align: center;
        }
        .download-style{
            color: #FF1493;
            font-size: 20px;
            padding-bottom: 15px;
        }
        #content{
            margin-top: 15px;
            padding-bottom: 30px;
            text-align: center;
            padding-left: 15px;
            padding-right: 15px;
            font-size: 16px;
            width:100%;
        }
        @media screen and (max-width: 1024px) {
            .left{
                clear: both;
                width: 100%;
            }
            .right {
                clear: both;
                width: 100%;
                padding-top: 15px;
            }
        }

        table[class*="container-for-gmail-android"] {
            min-width: 290px !important;
            width: 100% !important;
        }

        table[class="w320"] {
            width: 320px !important;
        }

        img[class="force-width-gmail"] {
            display: none !important;
            width: 0 !important;
            height: 0 !important;
        }

        .pull-right {
            text-align: right;
        }

        .mobile-header-padding-right {
            width: 290px;
            text-align: right;
            padding-left: 10px;
        }

        .mobile-header-padding-left {
            width: 290px;
            text-align: left;
            padding-left: 10px;
        }

    </style>
</head>
<body>
<div style="text-align: center;">
    <img width="500" src="http://138.197.11.186/woc/admin/images/01_Logo-on-white-background.jpg" alt="logo">
</div>
<div>
    <div >

        <div style="text-align: center;">
            <p id="content">{!! $message_body !!}</p>
        </div>


    </div>

</div>
<div>
    <table style="width: 100%; padding: 5px;">
        <tr>
            <td align="center" valign="top" width="100%" style="background-color: #f7f7f7; height: 100px;">
                <center>
                    <table cellspacing="0" cellpadding="0" class="w320">
                        <tr>
                            <td style="padding: 25px 0 25px; text-align: center;">
                                <strong>  www.test.com</strong><br />

                            </td>
                        </tr>

                    </table>
                </center>
            </td>
        </tr>
    </table>
</div>
</body>
</html>