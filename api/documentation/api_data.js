define({ "api": [
  {
    "type": "post",
    "url": "project/api/public/CategoryList",
    "title": "user for CategoryList",
    "name": "CategoryList",
    "group": "user",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Request-Header:",
          "content": "   {\nKey: Authorization\nValue: Bearer token\n}",
          "type": "json"
        },
        {
          "title": "Request-Body:",
          "content": "            {\n\"id\":12\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "          {\n   \"code\": \"200\",\n   \"message\": \"Record deleted successfully.\",\n   \"cause\": \"\",\n   \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/crudOprationController.php",
    "groupTitle": "user"
  },
  {
    "type": "post",
    "url": "project/api/public/categoryAdd",
    "title": "user for categoryAdd",
    "name": "categoryAdd",
    "group": "user",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Request-Header:",
          "content": "   {\nKey: Authorization\nValue: Bearer token\n}",
          "type": "json"
        },
        {
          "title": "Request-Body:",
          "content": "               //if you have NO child user\n    {\n\"child_name\":\"test11\"\n}\n    or\n\n    //if you have child user\n    {\n\"child_name\":\"test11\",\n\"parent_id\":1\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "             {\n    \"code\": \"200\",\n    \"message\": \"Record inserted successfully.\",\n    \"cause\": \"\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/crudOprationController.php",
    "groupTitle": "user"
  },
  {
    "type": "post",
    "url": "project/api/public/categoryDelete",
    "title": "user for categoryDelete",
    "name": "categoryDelete",
    "group": "user",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Request-Header:",
          "content": "   {\nKey: Authorization\nValue: Bearer token\n}",
          "type": "json"
        },
        {
          "title": "Request-Body:",
          "content": "             {\n\"id\":12\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "           {\n    \"code\": \"200\",\n    \"message\": \"Record deleted successfully.\",\n    \"cause\": \"\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/crudOprationController.php",
    "groupTitle": "user"
  },
  {
    "type": "post",
    "url": "project/api/public/categoryUpdate",
    "title": "user for categoryUpdate",
    "name": "categoryUpdate",
    "group": "user",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Request-Header:",
          "content": "   {\nKey: Authorization\nValue: Bearer token\n}",
          "type": "json"
        },
        {
          "title": "Request-Body:",
          "content": "              {\n\"child_name\":\"testttt\",\n\"id\":8\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "             {\n    \"code\": \"200\",\n    \"message\": \"Record updated successfully.\",\n    \"cause\": \"\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/crudOprationController.php",
    "groupTitle": "user"
  },
  {
    "type": "post",
    "url": "project/api/public/doLoginForUser",
    "title": "user for doLoginForUser",
    "name": "doLoginForUser",
    "group": "user",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Request-Header:",
          "content": "{}",
          "type": "json"
        },
        {
          "title": "Request-Body:",
          "content": "                {\n\"email_id\":\"ankita1@grr.la\",\n\"password\":\"demo@123\"\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "              {\n    \"code\": \"200\",\n    \"message\": \"Login Success.\",\n    \"cause\": \"\",\n    \"data\": {\n        \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQxLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L3Byb2plY3QvYXBpL3B1YmxpYy9kb0xvZ2luRm9yVXNlciIsImlhdCI6MTUxMDU3Mzg1MSwiZXhwIjoxNTEwNTc3NDUxLCJuYmYiOjE1MTA1NzM4NTEsImp0aSI6IldPQUVLbHBZS0x3NUNoVjgifQ.pfkTFgx-71fU2B8ikSL4e4aV_CAIMBH1FULM9HIMBAM\",\n        \"user_details\": [\n            {\n                \"id\": 41,\n                \"email_id\": \"ankita1@grr.la\"\n            }\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/loginController.php",
    "groupTitle": "user"
  },
  {
    "type": "post",
    "url": "project/api/public/userSignUp",
    "title": "user for userSignUp",
    "name": "userSignUp",
    "group": "user",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Request-Header:",
          "content": "{}",
          "type": "json"
        },
        {
          "title": "Request-Body:",
          "content": "            {\n\t\"email\":\"ankita1@grr.la\",\n\t\"password\":\"demo@123\"\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "          {\n    \"code\": \"200\",\n    \"message\": \"You will be receiving sms shortly.\",\n    \"cause\": \"\",\n    \"data\": {\n        \"user_reg_temp_id\": 16,\n        \"otp_token\": \"269817\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/RegisterController.php",
    "groupTitle": "user"
  },
  {
    "type": "post",
    "url": "project/api/public/verifyOTPforUser",
    "title": "user for verifyOTPforUser",
    "name": "verifyOTPforUser",
    "group": "user",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Request-Header:",
          "content": "{}",
          "type": "json"
        },
        {
          "title": "Request-Body:",
          "content": "            {\n\t\"user_reg_temp_id\":16,\n\t\"otp_token\":\"269817\"\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "          {\n    \"code\": \"200\",\n    \"message\": \"You have successfully SignUp.\",\n    \"cause\": \"\",\n    \"data\": {\n        \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQxLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L3Byb2plY3QvYXBpL3B1YmxpYy92ZXJpZnlPVFBmb3JVc2VyIiwiaWF0IjoxNTEwNTczNjg4LCJleHAiOjE1MTA1NzcyODgsIm5iZiI6MTUxMDU3MzY4OCwianRpIjoiaTRJV1lxaG1RUkQxcno2RSJ9.Xlm5WJbCtXR9adUvIo5YSVJZ2wk8HqeZiybD27EiE84\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/RegisterController.php",
    "groupTitle": "user"
  }
] });
