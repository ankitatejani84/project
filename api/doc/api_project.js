define({
  "name": "apidoc",
  "version": "5.3.26",
  "description": "apiDoc example project",
  "title": "Custom apiDoc browser title",
  "url": "",
  "header": {
    "title": "My own header title",
    "content": ""
  },
  "footer": {
    "title": "My own footer title",
    "content": ""
  },
  "order": [
    "GetUser",
    "PostUser"
  ],
  "template": {
    "withCompare": true,
    "withGenerator": true
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2017-03-17T11:29:35.755Z",
    "url": "http://apidocjs.com",
    "version": "0.17.5"
  }
});
