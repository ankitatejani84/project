-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 13, 2017 at 11:32 AM
-- Server version: 5.7.19
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `templet`
--

-- --------------------------------------------------------

--
-- Table structure for table `child`
--

DROP TABLE IF EXISTS `child`;
CREATE TABLE IF NOT EXISTS `child` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `child_name` varchar(65) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parents` varchar(65) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `child`
--

INSERT INTO `child` (`id`, `child_name`, `parent_id`, `parents`, `create_time`, `update_time`) VALUES
(7, 'test1', NULL, NULL, '2017-11-13 11:44:43', '2017-11-13 16:17:57'),
(8, 'test2', NULL, NULL, '2017-11-13 11:45:33', '2017-11-13 16:18:01'),
(9, 'test3', 7, '7', '2017-11-13 11:46:11', '2017-11-13 16:18:03'),
(10, 'test4', 9, '7.9', '2017-11-13 11:46:28', '2017-11-13 16:18:06'),
(11, 'test5', 10, '7.9.10', '2017-11-13 11:46:39', '2017-11-13 16:18:09'),
(17, 'test67', 9, '7.9', '2017-11-13 16:39:42', '2017-11-13 16:39:42'),
(16, 'test6', 11, '7.9.10.11', '2017-11-13 16:39:00', '2017-11-13 16:39:00');

-- --------------------------------------------------------

--
-- Table structure for table `child_parents`
--

DROP TABLE IF EXISTS `child_parents`;
CREATE TABLE IF NOT EXISTS `child_parents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `child_table_id` int(11) DEFAULT NULL,
  `parents` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `child_parents`
--

INSERT INTO `child_parents` (`id`, `child_table_id`, `parents`, `create_time`, `update_time`) VALUES
(1, 2, 1, '2017-10-14 14:35:05', '2017-10-14 14:35:05'),
(2, 1, NULL, '2017-10-14 15:08:27', '2017-10-14 15:08:27');

-- --------------------------------------------------------

--
-- Table structure for table `device_master`
--

DROP TABLE IF EXISTS `device_master`;
CREATE TABLE IF NOT EXISTS `device_master` (
  `device_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `device_reg_id` text NOT NULL,
  `device_platform` enum('android','ios','chrome','other') NOT NULL,
  `device_model_name` text COMMENT 'like: nexus 4, nexus 5',
  `device_vendor_name` text COMMENT 'like: LG',
  `device_os_version` text COMMENT 'like: for Android: 4.4,5.0',
  `device_udid` text NOT NULL COMMENT 'udid for device',
  `device_resolution` text COMMENT 'like: width*height',
  `device_carrier` text COMMENT 'like: vodafone',
  `device_country_code` varchar(10) DEFAULT NULL COMMENT 'like: +1 for us',
  `device_language` varchar(50) DEFAULT NULL COMMENT 'like: en for english',
  `device_local_code` varchar(10) DEFAULT NULL COMMENT 'like: 411001',
  `device_default_time_zone` varchar(25) DEFAULT NULL COMMENT 'like: GMT+09:30',
  `device_library_version` varchar(10) DEFAULT NULL COMMENT 'like: 1 (it is ob lib version)',
  `device_application_version` varchar(10) DEFAULT NULL COMMENT 'Device app version',
  `device_type` varchar(25) DEFAULT NULL COMMENT 'like: phone, tablet',
  `device_registration_date` varchar(30) DEFAULT NULL COMMENT 'time of device when it registred',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `attribute1` text,
  `attribute2` text,
  `attribute3` text,
  `attribute4` text,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `device_master`
--
DROP TRIGGER IF EXISTS `device_master_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `device_master_BEFORE_INSERT` BEFORE INSERT ON `device_master` FOR EACH ROW BEGIN
SET NEW.create_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `device_master_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `device_master_BEFORE_UPDATE` BEFORE UPDATE ON `device_master` FOR EACH ROW BEGIN
SET NEW.update_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs_detail`
--

DROP TABLE IF EXISTS `failed_jobs_detail`;
CREATE TABLE IF NOT EXISTS `failed_jobs_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `failed_job_id` int(11) DEFAULT NULL,
  `user_id` varchar(254) DEFAULT NULL,
  `api_name` text,
  `api_description` text,
  `job_name` varchar(254) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `attribute1` text,
  `attribute2` text,
  `attribute3` text,
  `attribute4` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'test 2', 'test__________2', '2017-09-12 22:30:00', '2017-09-13 08:00:30'),
(2, 'test 1', 'test ----------1', '2017-09-13 07:58:14', '2017-09-13 08:00:13'),
(3, 'test 3', 'test------4', '2017-09-13 08:00:42', '2017-09-13 08:00:42'),
(4, 'test 4', 'test-----4', '2017-09-13 08:02:30', '2017-09-13 08:02:30'),
(5, 'test 5', 'dcvcbdc', '2017-09-14 01:23:59', '2017-09-14 01:23:59'),
(6, 'test 22', 'vb nv', '2017-09-14 04:34:47', '2017-09-14 04:34:47');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_02_10_131631_create_jobs_table', 2),
(4, '2017_02_10_131641_create_failed_jobs_table', 2),
(5, '2017_09_11_125300_create_products_table', 3),
(6, '2017_09_13_130005_create_items_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `otp_codes`
--

DROP TABLE IF EXISTS `otp_codes`;
CREATE TABLE IF NOT EXISTS `otp_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(254) NOT NULL,
  `otp_token` varchar(10) NOT NULL,
  `otp_token_expire` timestamp NULL DEFAULT NULL,
  `user_registration_temp_id` int(11) DEFAULT NULL COMMENT 'NULL if OTP is for change password.',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `attribute1` text,
  `attribute2` text,
  `attribute3` text,
  `attribute4` text,
  PRIMARY KEY (`id`),
  KEY `from_otp_codes_to_user_registration_temp_idx` (`user_registration_temp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `otp_codes`
--

INSERT INTO `otp_codes` (`id`, `user_name`, `otp_token`, `otp_token_expire`, `user_registration_temp_id`, `create_time`, `update_time`, `attribute1`, `attribute2`, `attribute3`, `attribute4`) VALUES
(1, 'demo@grr.la', '986573', '2017-09-12 06:35:10', 1, '2017-09-12 11:05:10', '2017-09-12 11:05:10', NULL, NULL, NULL, NULL),
(2, 'demo@grr.la', '946152', '2017-09-12 06:42:49', 2, '2017-09-12 11:12:49', '2017-09-12 11:12:49', NULL, NULL, NULL, NULL),
(3, 'demo@grr.la', '658217', '2017-09-12 06:51:31', 3, '2017-09-12 11:21:31', '2017-09-12 11:21:31', NULL, NULL, NULL, NULL),
(4, 'demo1@grr.la', '270549', '2017-09-12 08:18:16', 4, '2017-09-12 12:48:16', '2017-09-12 12:48:16', NULL, NULL, NULL, NULL),
(5, 'demo2@grr.la', '124063', '2017-09-12 08:28:28', 5, '2017-09-12 12:58:28', '2017-09-12 12:58:28', NULL, NULL, NULL, NULL),
(6, 'demo4@grr.la', '692873', '2017-09-12 08:30:14', 6, '2017-09-12 13:00:14', '2017-09-12 13:00:14', NULL, NULL, NULL, NULL),
(7, 'ankita@grr.la', '153726', '2017-09-12 08:44:52', 7, '2017-09-12 13:14:52', '2017-09-12 13:14:52', NULL, NULL, NULL, NULL),
(8, 'ankita@grr.la', '075432', '2017-10-14 02:49:04', 12, '2017-10-14 07:19:04', '2017-10-14 07:19:04', NULL, NULL, NULL, NULL),
(9, 'ankita@grr.la', '475613', '2017-10-14 02:50:03', 13, '2017-10-14 07:20:03', '2017-10-14 07:20:03', NULL, NULL, NULL, NULL),
(10, 'ankita@grr.la', '362145', '2017-10-14 02:50:38', 14, '2017-10-14 07:20:38', '2017-10-14 07:20:38', NULL, NULL, NULL, NULL),
(11, 'ankita@grr.la', '582431', '2017-10-14 02:52:29', 15, '2017-10-14 07:22:29', '2017-10-14 07:22:29', NULL, NULL, NULL, NULL);

--
-- Triggers `otp_codes`
--
DROP TRIGGER IF EXISTS `otp_codes_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `otp_codes_BEFORE_INSERT` BEFORE INSERT ON `otp_codes` FOR EACH ROW BEGIN
SET NEW.create_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `otp_codes_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `otp_codes_BEFORE_UPDATE` BEFORE UPDATE ON `otp_codes` FOR EACH ROW BEGIN
SET NEW.update_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin_permission', NULL, NULL, '2017-02-11 05:18:47', '2017-02-11 05:18:47'),
(2, 'user_permission', NULL, NULL, '2017-02-11 05:19:01', '2017-02-11 05:19:01');

--
-- Triggers `permissions`
--
DROP TRIGGER IF EXISTS `permissions_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `permissions_BEFORE_INSERT` BEFORE INSERT ON `permissions` FOR EACH ROW BEGIN
SET NEW.created_at = CURRENT_TIMESTAMP;
SET NEW.updated_at = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `from_permission_role_to_permission_idx` (`permission_id`),
  KEY `from_permission_role_to_roles_idx` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', NULL, '2017-02-11 05:17:10', '2017-02-11 05:17:10'),
(2, 'user', 'user', NULL, '2017-02-11 05:17:25', '2017-02-11 05:17:25');

--
-- Triggers `roles`
--
DROP TRIGGER IF EXISTS `roles_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `roles_BEFORE_INSERT` BEFORE INSERT ON `roles` FOR EACH ROW BEGIN
SET NEW.created_at = CURRENT_TIMESTAMP;
SET NEW.updated_at = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `from_role_user_to_user_master_idx` (`user_id`),
  KEY `from_role_user_to_roles_idx` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(18, 2),
(28, 2),
(30, 2),
(31, 2),
(40, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sent_notification_detail`
--

DROP TABLE IF EXISTS `sent_notification_detail`;
CREATE TABLE IF NOT EXISTS `sent_notification_detail` (
  `ntf_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `ntf_id` int(11) NOT NULL,
  `device_platform` text NOT NULL,
  `ntf_sent` int(11) NOT NULL,
  `ntf_success` int(11) NOT NULL,
  `ntf_failure` int(11) NOT NULL,
  `ntf_canonical` int(11) NOT NULL,
  `ntf_received` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `attribute1` text,
  `attribute2` text,
  `attribute3` text,
  `attribute4` text,
  PRIMARY KEY (`ntf_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sent_notification_detail`
--
DROP TRIGGER IF EXISTS `sent_notification_detail_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `sent_notification_detail_BEFORE_INSERT` BEFORE INSERT ON `sent_notification_detail` FOR EACH ROW BEGIN
SET NEW.create_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `sent_notification_detail_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `sent_notification_detail_BEFORE_UPDATE` BEFORE UPDATE ON `sent_notification_detail` FOR EACH ROW BEGIN
SET NEW.update_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sent_notification_logs`
--

DROP TABLE IF EXISTS `sent_notification_logs`;
CREATE TABLE IF NOT EXISTS `sent_notification_logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `ntf_id` int(11) NOT NULL,
  `request_header` longtext NOT NULL,
  `request_body` longtext NOT NULL,
  `response_header` longtext NOT NULL,
  `response_body` longtext NOT NULL,
  `response_header_code` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `attribute1` text,
  `attribute2` text,
  `attribute3` text,
  `attribute4` text,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sent_notification_logs`
--
DROP TRIGGER IF EXISTS `sent_notification_logs_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `sent_notification_logs_BEFORE_INSERT` BEFORE INSERT ON `sent_notification_logs` FOR EACH ROW BEGIN
SET NEW.create_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `sent_notification_logs_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `sent_notification_logs_BEFORE_UPDATE` BEFORE UPDATE ON `sent_notification_logs` FOR EACH ROW BEGIN
SET NEW.update_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sent_notification_master`
--

DROP TABLE IF EXISTS `sent_notification_master`;
CREATE TABLE IF NOT EXISTS `sent_notification_master` (
  `ntf_id` int(11) NOT NULL AUTO_INCREMENT,
  `ntf_title` text NOT NULL,
  `ntf_message` text NOT NULL,
  `ntf_icon_path` text NOT NULL,
  `ntf_type` enum('alert','update','url') NOT NULL COMMENT 'like: update, alert',
  `ntf_filter` text NOT NULL COMMENT 'notification filter detail like sent only to  OS:4.1',
  `ntf_total_device` int(11) NOT NULL COMMENT 'count of total device selected for notification.',
  `ntf_status` int(11) NOT NULL COMMENT 'response code from server like 200,401',
  `was_scheduled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: was not scheduled  1: was scheduled',
  `is_active` tinyint(1) DEFAULT '1',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ntf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sent_notification_master`
--
DROP TRIGGER IF EXISTS `sent_notification_master_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `sent_notification_master_BEFORE_INSERT` BEFORE INSERT ON `sent_notification_master` FOR EACH ROW BEGIN
SET NEW.create_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `sent_notification_master_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `sent_notification_master_BEFORE_UPDATE` BEFORE UPDATE ON `sent_notification_master` FOR EACH ROW BEGIN
SET NEW.update_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@123', '$2y$10$rZPexBrq7PGZf7bxu4sWTex8deKk4/32PtWkjPRrOk/Mx7wXtKvEG', NULL, '2017-02-09 18:30:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
CREATE TABLE IF NOT EXISTS `user_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `phone_number_1` varchar(15) DEFAULT NULL,
  `profile_img` text,
  `about_me` text,
  `phone_number` varchar(15) DEFAULT NULL,
  `address_line_1` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip_code` varchar(6) DEFAULT NULL,
  `contry` varchar(255) DEFAULT NULL,
  `bg_color` varchar(8) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `attribute1` text,
  `attribute2` text,
  `attribute3` text,
  `attribute4` text,
  PRIMARY KEY (`id`),
  KEY `from_user_detail_to_user_master_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `first_name`, `last_name`, `phone_number_1`, `profile_img`, `about_me`, `phone_number`, `address_line_1`, `city`, `state`, `zip_code`, `contry`, `bg_color`, `create_time`, `update_time`, `attribute1`, `attribute2`, `attribute3`, `attribute4`) VALUES
(1, 1, 'admin', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-11 05:26:18', '2017-02-11 05:26:18', NULL, NULL, NULL, NULL),
(18, 18, 'ankita', 'tejani', NULL, '59b7dba9e5c7b_user_que_ans_img_1505221545.jpg', 'laravel devloper', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-12 11:35:26', '2017-09-12 13:05:45', NULL, NULL, NULL, NULL),
(30, 30, 'ankita', 'tejani', NULL, NULL, 'laravel devloper', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-12 12:59:11', '2017-09-12 12:59:11', NULL, NULL, NULL, NULL),
(31, 31, 'ankita', 'tejani', NULL, NULL, 'laravel devloper', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-12 13:00:24', '2017-09-12 13:00:24', NULL, NULL, NULL, NULL),
(38, 40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-14 07:53:41', '2017-10-14 07:53:41', NULL, NULL, NULL, NULL);

--
-- Triggers `user_details`
--
DROP TRIGGER IF EXISTS `user_detail_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `user_detail_BEFORE_INSERT` BEFORE INSERT ON `user_details` FOR EACH ROW BEGIN
SET NEW.create_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `user_detail_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `user_detail_BEFORE_UPDATE` BEFORE UPDATE ON `user_details` FOR EACH ROW BEGIN
SET NEW.update_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_master`
--

DROP TABLE IF EXISTS `user_master`;
CREATE TABLE IF NOT EXISTS `user_master` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email_id` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `social_uid` varchar(255) DEFAULT NULL,
  `signup_type` int(11) DEFAULT NULL COMMENT '1 = email,\n2 = facebook,\n3 = twitter\n4 = gmail',
  `profile_setup` tinyint(1) DEFAULT '0',
  `is_active` tinyint(1) DEFAULT NULL,
  `is_verify` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `attribute1` text,
  `attribute2` text,
  `attribute3` text,
  `attribute4` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_id_UNIQUE` (`email_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_master`
--

INSERT INTO `user_master` (`id`, `email_id`, `password`, `social_uid`, `signup_type`, `profile_setup`, `is_active`, `is_verify`, `create_time`, `update_time`, `attribute1`, `attribute2`, `attribute3`, `attribute4`) VALUES
(1, 'admin@grr.la', '$2y$10$rZPexBrq7PGZf7bxu4sWTex8deKk4/32PtWkjPRrOk/Mx7wXtKvEG', NULL, NULL, 0, NULL, 0, '2017-02-11 04:58:14', '2017-03-18 09:04:42', NULL, NULL, NULL, NULL),
(18, 'demo@grr.la', '$2y$10$QY0iITyqItKb949rT/bWaOkTaaenRjZ9HjIPDXpazWS/3pnzEIhHu', NULL, NULL, 1, 1, 0, '2017-09-12 11:35:26', '2017-09-12 12:46:02', NULL, NULL, NULL, NULL),
(28, 'demo1@grr.la', '$2y$10$nMWBbuNN7C8jWo92xzvh6exmqt3CwZ/RVVez8GpEzwX.9j8lKI1ha', NULL, NULL, 1, 1, 0, '2017-09-12 12:55:25', '2017-09-12 12:55:25', NULL, NULL, NULL, NULL),
(30, 'demo2@grr.la', '$2y$10$W9C.gEyADoVWJqJAGTJyRuASxPNYZzGpHBRJWY9NuYoa7Cm6DJ6yu', NULL, NULL, 1, 1, 0, '2017-09-12 12:59:11', '2017-09-12 12:59:11', NULL, NULL, NULL, NULL),
(31, 'demo4@grr.la', '$2y$10$eOfINzoVcCUi.7dP9uGI3uwkPp/MGjz.R0A8egBSMgtdOGUlET7.m', NULL, NULL, 1, 1, 0, '2017-09-12 13:00:24', '2017-09-12 13:00:24', NULL, NULL, NULL, NULL),
(40, 'ankita@grr.la', '$2y$10$yAXzLtOwyro3vEuhrOUgzukQ9CQ3JwZwfmszrSCbkhD2s0fx0bJZa', NULL, NULL, 1, 1, 0, '2017-10-14 07:53:41', '2017-10-14 07:53:41', NULL, NULL, NULL, NULL);

--
-- Triggers `user_master`
--
DROP TRIGGER IF EXISTS `user_master_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `user_master_BEFORE_INSERT` BEFORE INSERT ON `user_master` FOR EACH ROW BEGIN
SET NEW.create_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `user_master_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `user_master_BEFORE_UPDATE` BEFORE UPDATE ON `user_master` FOR EACH ROW BEGIN
SET NEW.update_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_pwd_reset_token_master`
--

DROP TABLE IF EXISTS `user_pwd_reset_token_master`;
CREATE TABLE IF NOT EXISTS `user_pwd_reset_token_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_id` varchar(254) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `reset_token` varchar(100) DEFAULT NULL,
  `reset_token_expire` timestamp NULL DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `attribute1` text,
  `attribute2` text,
  `attribute3` text,
  `attribute4` text,
  PRIMARY KEY (`id`),
  KEY `from_user_pwd_reset_to_user_master_idx` (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `user_pwd_reset_token_master`
--
DROP TRIGGER IF EXISTS `user_pwd_reset_token_master_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `user_pwd_reset_token_master_BEFORE_INSERT` BEFORE INSERT ON `user_pwd_reset_token_master` FOR EACH ROW BEGIN
SET NEW.create_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `user_pwd_reset_token_master_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `user_pwd_reset_token_master_BEFORE_UPDATE` BEFORE UPDATE ON `user_pwd_reset_token_master` FOR EACH ROW BEGIN
SET NEW.update_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_registration_temp`
--

DROP TABLE IF EXISTS `user_registration_temp`;
CREATE TABLE IF NOT EXISTS `user_registration_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(254) DEFAULT NULL,
  `request_json` text,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `attribute1` text,
  `attribute2` text,
  `attribute3` text,
  `attribute4` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_registration_temp`
--

INSERT INTO `user_registration_temp` (`id`, `user_name`, `request_json`, `create_time`, `update_time`, `attribute1`, `attribute2`, `attribute3`, `attribute4`) VALUES
(1, 'demo@grr.la', '{\"password\":\"demo@123\",\"email_id\":\"demo@grr.la\",\"first_name\":\"ankita\",\"last_name\":\"tejani\",\"about_me\":\"laravel devloper\"}', '2017-09-12 11:05:10', '2017-09-12 11:05:10', NULL, NULL, NULL, NULL),
(2, 'demo@grr.la', '{\"password\":\"demo@123\",\"email_id\":\"demo@grr.la\"}', '2017-09-12 11:12:49', '2017-09-12 11:12:49', NULL, NULL, NULL, NULL),
(3, 'demo@grr.la', '{\"password\":\"demo@123\",\"email_id\":\"demo@grr.la\",\"first_name\":\"ankita\",\"last_name\":\"tejani\",\"about_me\":\"laravel devloper\"}', '2017-09-12 11:21:31', '2017-09-12 11:21:31', NULL, NULL, NULL, NULL),
(5, 'demo1@grr.la', '{\"password\":\"demo@123\",\"email_id\":\"demo1@grr.la\",\"first_name\":\"ankita\",\"last_name\":\"tejani\",\"about_me\":\"laravel devloper\"}', '2017-09-12 12:48:16', '2017-09-12 12:59:35', NULL, NULL, NULL, NULL),
(6, 'demo4@grr.la', '{\"password\":\"demo@123\",\"email_id\":\"demo4@grr.la\",\"first_name\":\"ankita\",\"last_name\":\"tejani\",\"about_me\":\"laravel devloper\"}', '2017-09-12 13:00:14', '2017-09-12 13:00:14', NULL, NULL, NULL, NULL),
(7, 'ankita@grr.la', '{\"password\":\"demo@123\",\"email_id\":\"ankita@grr.la\",\"first_name\":\"ankita\",\"last_name\":\"tejani\",\"about_me\":\"laravel devloper\"}', '2017-09-12 13:14:52', '2017-09-12 13:14:52', NULL, NULL, NULL, NULL),
(12, 'ankita@grr.la', '{\"email\":\"ankita@grr.la\",\"password\":\"demo@123\"}', '2017-10-14 07:19:04', '2017-10-14 07:19:04', NULL, NULL, NULL, NULL),
(13, 'ankita@grr.la', '{\"email\":\"ankita@grr.la\",\"password\":\"demo@123\"}', '2017-10-14 07:20:03', '2017-10-14 07:20:03', NULL, NULL, NULL, NULL),
(14, 'ankita@grr.la', '{\"email\":\"ankita@grr.la\",\"password\":\"demo@123\"}', '2017-10-14 07:20:38', '2017-10-14 07:20:38', NULL, NULL, NULL, NULL),
(15, 'ankita@grr.la', '{\"email\":\"ankita@grr.la\",\"password\":\"demo@123\"}', '2017-10-14 07:22:29', '2017-10-14 07:22:29', NULL, NULL, NULL, NULL);

--
-- Triggers `user_registration_temp`
--
DROP TRIGGER IF EXISTS `user_registration_temp_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `user_registration_temp_BEFORE_INSERT` BEFORE INSERT ON `user_registration_temp` FOR EACH ROW BEGIN
SET NEW.create_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `user_registration_temp_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `user_registration_temp_BEFORE_UPDATE` BEFORE UPDATE ON `user_registration_temp` FOR EACH ROW BEGIN
SET NEW.update_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_session`
--

DROP TABLE IF EXISTS `user_session`;
CREATE TABLE IF NOT EXISTS `user_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `token` text,
  `device_udid` text,
  `device_id` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `attribute1` text,
  `attribute2` text,
  `attribute3` text,
  `attribute4` text,
  PRIMARY KEY (`id`),
  KEY `from_user_session_to_user_master_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_session`
--

INSERT INTO `user_session` (`id`, `user_id`, `token`, `device_udid`, `device_id`, `create_time`, `update_time`, `attribute1`, `attribute2`, `attribute3`, `attribute4`) VALUES
(1, 18, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE4LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L3Rlc3QvYXBpL3B1YmxpYy9kb0xvZ2luRm9yVXNlciIsImlhdCI6MTUwNTIyMDM3MSwiZXhwIjoxNTA1MjIzOTcxLCJuYmYiOjE1MDUyMjAzNzEsImp0aSI6IkE1S3Z6Rk9oRDRaT2E5ZHIifQ.kOncKbl5xcOM2c1ywcPw3un70JyFNFYY21Qu_BWaDS8', 'web', NULL, '2017-09-12 12:45:54', '2017-09-12 12:46:11', NULL, NULL, NULL, NULL),
(11, 28, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI4LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L3Rlc3QvYXBpL3B1YmxpYy92ZXJpZnlPVFBfRm9yX1VzZXIiLCJpYXQiOjE1MDUyMjA5MjUsImV4cCI6MTUwNTIyNDUyNSwibmJmIjoxNTA1MjIwOTI1LCJqdGkiOiI5SVJ1c0x1ZTl3OEpnRkVQIn0.GiTckLf_uYnkSqq2SuFS67rWLMwK4VX8x0F5an36AW4', '2017-09-12 12:55:25', NULL, '2017-09-12 12:55:25', '2017-09-12 12:55:25', NULL, NULL, NULL, NULL),
(13, 30, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L3Rlc3QvYXBpL3B1YmxpYy92ZXJpZnlPVFBfRm9yX1VzZXIiLCJpYXQiOjE1MDUyMjExNTEsImV4cCI6MTUwNTIyNDc1MSwibmJmIjoxNTA1MjIxMTUxLCJqdGkiOiIxelpVakZyUnVzVVVVMHVGIn0.ZoNBvpR1sjOh5qgeT5cxDyJbn962Ae4ofN4drMTgzHM', '2017-09-12 12:59:11', NULL, '2017-09-12 12:59:11', '2017-09-12 12:59:11', NULL, NULL, NULL, NULL),
(14, 31, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMxLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L3Rlc3QvYXBpL3B1YmxpYy92ZXJpZnlPVFBfRm9yX1VzZXIiLCJpYXQiOjE1MDUyMjEyMjQsImV4cCI6MTUwNTIyNDgyNCwibmJmIjoxNTA1MjIxMjI0LCJqdGkiOiIzaFdSbUhuemdjNlV6VVIyIn0.RMlqe3QFs6Z-jsYsTKJKKep99PVImgaY-BIkfV5kgc0', '2017-09-12 01:00:24', NULL, '2017-09-12 13:00:24', '2017-09-12 13:00:24', NULL, NULL, NULL, NULL),
(15, 40, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L3RlbXBsZXRfNV80L2FwaS9wdWJsaWMvZG9Mb2dpbkZvclVzZXIiLCJpYXQiOjE1MDc5NjgxMTksImV4cCI6MTUwNzk3MTcxOSwibmJmIjoxNTA3OTY4MTE5LCJqdGkiOiJUcGFaV1pJTHI5U0J6ZnVHIn0.2lU9lJKAW9eWaePKyKn7RuccJrd3Uu2wOgWzEZeyCaw', NULL, NULL, '2017-10-14 07:53:41', '2017-10-14 08:01:59', NULL, NULL, NULL, NULL);

--
-- Triggers `user_session`
--
DROP TRIGGER IF EXISTS `user_session_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `user_session_BEFORE_INSERT` BEFORE INSERT ON `user_session` FOR EACH ROW BEGIN
SET NEW.create_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `user_session_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `user_session_BEFORE_UPDATE` BEFORE UPDATE ON `user_session` FOR EACH ROW BEGIN
SET NEW.update_time = CURRENT_TIMESTAMP;
END
$$
DELIMITER ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `from_permission_role_to_permission` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `from_permission_role_to_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `user_id_to_blistek_user_master_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `from_user_detail_to_user_master` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_pwd_reset_token_master`
--
ALTER TABLE `user_pwd_reset_token_master`
  ADD CONSTRAINT `from_user_pwd_reset_to_user_master` FOREIGN KEY (`email_id`) REFERENCES `user_master` (`email_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_session`
--
ALTER TABLE `user_session`
  ADD CONSTRAINT `from_user_session_to_user_master` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
