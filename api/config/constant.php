<?php
/**
 * Created by PhpStorm.
 * User: ob-2
 * Date: 1/27/2016
 * Time: 3:29 PM
 */
return [
'ROLE_ID_FOR_USER'=>2,
   /* //////////////////////////////Change Server Configuration////////////////////////////////////////////////////////


                   'ACTIVATION_LINK_PATH'=>'http://'.$_SERVER['HTTP_HOST'].'/'.basename(dirname(dirname(__DIR__))),     //local

                   'FORGOT_PASSWORD_SVN_PATH' => 'http://'.$_SERVER['HTTP_HOST'].'/'.basename(dirname(dirname(__DIR__))).'/api/public/activeProfile/',   //local

                   'XMPP_HOST' => '192.168.0.106', //local



//                     'ACTIVATION_LINK_PATH'=>'http://'.$_SERVER['HTTP_HOST'],   //server
//
//                    'FORGOT_PASSWORD_SVN_PATH' => 'http://'.$_SERVER['HTTP_HOST'].'/api/public/activeProfile/',           //server
//
//                    'XMPP_HOST' => '35.161.72.141', //live aws

                  // 'XMPP_HOST' => '138.197.11.186', //live


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////




//    'APPLICATION_LOGO_PATH' => 'http://'.$_SERVER['HTTP_HOST'].'/'.basename(dirname(dirname(__DIR__))).'/images/logo.png', //'.$_SERVER['HTTP_HOST'].'
    'APPLICATION_LOGO_PATH' => 'http://localhost/'.basename(dirname(dirname(__DIR__))).'/images/logo.png', //localhost
   // 'APPLICATION_LOGO_PATH' => 'http://localhost/'.basename(dirname(dirname(__DIR__))).'/images/logo.png', //server
    'ADMIN_EMAIL_ID' => 'alagiyanirav@gmail.com',
    'SYSADMIN_EMAIL_ID' => 'alagiyanirav@gmail.com',

    'BOOK_SERVICE_NOTIFICATION_TITLE' => 'Service Booked',


    'CACHE_ENABLE' => true,
    'CACHE_EXPIRATION_TIME' => '1440', // 1 day = 24 hours = 1440 minutes
    'CACHING_TIME' => '1',

    'COLOR_FOR_IS_SUPPORT' => '#ef3f4a',
    'COLOR_FOR_IS__NOT_SUPPORT' => '#43525a',
    'COLOR_FOR_IS__OWNER' => '#3fafef',

    'DATE_FORMAT' => 'Y-m-d H:i:s',
    'DISTANCE_LIMIT' => 20,

    'EMAIL_LOGO' => '',
    'EMAIL_LOGO_PATH' => '',
    'EXCEPTION_ERROR' => basename(dirname(dirname(__DIR__))).' is unable to ',
    'EMAIL_BLAST_SUBJECT' => 'SuiteScene Offer',



    'GCM_NOTIFICATION_URL' => 'https://fcm.googleapis.com/fcm/send',
    'GCM_SERVER_KEY' => 'AAAARrposm8:APA91bGiCH8kpJn0ieaf06za2GI-M-BCTa8dXEsUiPALqTilcLzygtcgpIRZmC-gkQLr5OoEk57zytGC1sngPWPkKFVc-MWoWqd8Anxvzgo32STy8-XGEghOLHUHv_gcsYPIAva1jssen8gAITiDsc2u3obRpkwFBA',
    'GCM_SENDER_ID' => '256543862168',
    'GUEST_USER_UD' => 'guest@gmail.com',
    'GUEST_PASSWORD' => 'guest@123',

    'IS_SUPPORTING' => 'WALL OF COMPLIMENTS',
    'IS_UN_SUPPORTING' => 'WALL OF COMPLIMENTS',
    'IS_APPLAUD_ADD' => 'WALL OF COMPLIMENTS',
    'IS_PRIVATE_CANCEL' => 'WALL OF COMPLIMENTS',
    'IS_PRIVATE_ADD' => 'WALL OF COMPLIMENTS',
    'IS_SENT_THANK_YOU' => 'WALL OF COMPLIMENTS',
    'IS_ABUSE' => 'WALL OF COMPLIMENTS',
    'IS_block' => 'WALL OF COMPLIMENTS',
    'IS_PROFILE_ACTIVE' => 'WALL OF COMPLIMENTS',
    'LOGOUT_TITLE' => 'WALL OF COMPLIMENTS',



    'MINIMUM_ITEM_REQUIRED_MESSAGE' => 'Minimum 10 item_limit is required.',
    'MSG91_AUTH_KEY'=>'107943A5N6HPHr56ebd84d',
    'MSG91_SENDER_ID'=>'SuiteScene',

    'NOTIFICATION_TYPE_FOR_SEND_COMPLIMENT'=>1,
    'NOTIFICATION_TYPE_FOR_IS_SUPPORTING'=>2,
    'NOTIFICATION_TYPE_FOR_IS_UN_SUPPORTING'=>3,
    'NOTIFICATION_TYPE_FOR_IS_APPLAUD'=>4,
    'NOTIFICATION_TYPE_FOR_IS_PRIVATE_CANCEL'=>5,
    'NOTIFICATION_TYPE_FOR_IS_PRIVATE_ADD'=>6,
    'NOTIFICATION_TYPE_FOR_IS_THANK_YOU'=>7,
    'NOTIFICATION_TYPE_FOR_IS_ABUSE'=>8,
    'NOTIFICATION_TYPE_FOR_IS_BLOCK'=>9,
    'NOTIFICATION_TYPE_FOR_IS_UN_BLOCK'=>10,
    'NOTIFICATION_UPDATE_PROFILE'=>11,
    'CONST_NOTIFICATION_COMPLIMENT_SENT'=> 12,
    'CONST_NOTIFICATION_THANK_YOU_SENT'=> 13,
    'CONST_NOTIFICATION_YOU_SUPPORT_OTHER'=> 14,
    'CONST_NOTIFICATION_YOU_APPLAUD'=> 15,
    'CONST_NOTIFICATION_YOU_REPORT_ABUSE'=> 16,
    'CONST_NOTIFICATION_FOR_LOGOUT'=> 17,
    'NOTIFICATION_TYPE_FOR_SUPPORTER_USER'=> 18,
    'NOTIFICATION_TYPE_FOR_OTHER_DEVICE_LOGOUT'=> 19,


    'OTP_EXPIRATION_TIME' => '3',
    'OPTIMUMBREW_LOGO_PATH' => 'http://optimumbrew.com/images/logo/optimumbrew_logo.png',
    'OTHER_CATEGORY_ID' => 5,

    'PAGINATION_ITEM_COUNT' => '20',
    'PAGINATION_ITEM_COUNT_FOR_SYNC' => '500',
    'PAGINATION_ITEM_COUNT_FOR_ADMIN' => '30',
    'PROJECT_NAME' => 'WOC',


    'RESPONSE_HEADER_CACHE' => 'max-age=2592000',
    'ROLE_FOR_ADMIN' => 'admin',
    'ROLE_FOR_INDI_USER' => 'individual_user',
    'ROLE_FOR_LEGAL_USER' => 'legal_user',
    'ROLE_FOR_PUBLIC_FIGURE_USER' => 'public_figure_user',

    'SEND_COMPLIMENT' => 'WALL OF COMPLIMENTS',

    'COMPRESSED_IMAGES_DIRECTORY' => '/image_bucket/compressed/',
    'ORIGINAL_IMAGES_DIRECTORY' => '/image_bucket/original/',
    'THUMBNAIL_IMAGES_DIRECTORY' => '/image_bucket/thumbnail/',
    'ORIGINAL_VIDEO_DIRECTORY' => '/image_bucket/video/',

    'TOTAL_ORDER_AMOUNT' => 10,
    'THUMBNAIL_HEIGHT' => 240,
    'THUMBNAIL_WIDTH' => 320,


    'XMPP_DOMAIN' => 'localhost',
    'XMPP_PORT' => '5222',
    'XMPP_USER' => 'system_admin',
    'XMPP_PASSWORD' => 'system_admin',
    'XMPP_RESOURCE' => 'XMPPHP',
    'XMPP_REST_KEY' => 'qkCvTK8SCkkga5c8'*/

];