<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::post('doLoginForUser', 'demo@doLoginForUser');
Route::group(['prefix' => '', 'middleware' => ['ability:user,user_permission']], function()
{
    Route::post('activateUser', 'demo@activateUser');
});


 Route::post('userSignUp', 'RegisterController@userSignUp');
 Route::post('verifyOTPforUser', 'RegisterController@verifyOTPforUser');
 Route::post('doLoginForUser', 'loginController@doLoginForUser');
 Route::post('categoryAdd', 'crudOprationController@categoryAdd');
 Route::post('categoryUpdate', 'crudOprationController@categoryUpdate');
 Route::post('categoryDelete', 'crudOprationController@categoryDelete');
 Route::post('CategoryList', 'crudOprationController@CategoryList');
 Route::post('demo', 'crudOprationController@demo');